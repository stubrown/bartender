package com.example.solarmojo13.bartender;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;
@SuppressWarnings("serial")
public class BluetoothConnectionService {
    public static final String TAG = "BluetoothConnectionServ";

    //unique ID to identify service
    private UUID bartenderUUID;
    private static final String UUID_DEFAULT_END = "55-9e19-4040-a26b-886edb003fa6";
    //private static final UUID DEFAULT_UUID = UUID.fromString("42679155-9e19-4040-a26b-886edb003fa6");

    Context mContext;
    private final BluetoothAdapter mBluetoothAdapter;
    //handles connecting devices
    private AcceptThread acceptThread;
    //handles communication once devices are connected
    private ConnectedThread connectedThread;


    public BluetoothConnectionService(Context context, String UUID_start) {
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bartenderUUID = UUID.fromString(UUID_start + UUID_DEFAULT_END);
        start();
    }

    //Starts AcceptThread
    public synchronized void start() {
        Log.d(TAG, "start");

        if (acceptThread == null) {
            acceptThread = new AcceptThread();
            acceptThread.start();
        }
    }

    //Start ConnectThread once AcceptThread is finished
    private void connected(BluetoothSocket mmSocket) {
        Log.d(TAG, "connected: Starting.");

        acceptThread.cancel();
        connectedThread = new ConnectedThread(mmSocket);
        connectedThread.start();
        sendConnectedBroadcast();
    }

    private void sendConnectedBroadcast() {
        Intent intent = new Intent();
        intent.setAction("com.example.solarmojo13.bartender.ACTION_BLUETOOTH_CONNECTED");
        intent.putExtra("Connected", true);
        mContext.sendBroadcast(intent);
    }

    //call from main
    public void write(byte[] out) {
        Log.d(TAG, "write: Write Called.");
        if (connectedThread !=null) {
            connectedThread.write(out);
        }
    }

    //call from main
    public void cancel() {
        if (acceptThread!=null) {
            acceptThread.cancel();
        }
        if (connectedThread!=null) {
            connectedThread.cancel();
        }
    }


    private class AcceptThread extends Thread {
        private String name;
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;
            name = mBluetoothAdapter.getName() + " HotBar Service";

            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(name, bartenderUUID);
            } catch (IOException e) {
                Log.e(TAG, "Socket's listen() method failed", e);
            }

            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "run: AcceptThread Running.");
            BluetoothSocket socket = null;

            try {
                Log.d(TAG, "run: RFCOMM server socket start...");
                //this will only continue after a success or an exception
                socket = mmServerSocket.accept();
                Log.d(TAG, "run: RFCOMM server socket accepted connection.");
            } catch (IOException e) {
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());
            }

            if (socket!=null) {
                connected(socket);
            }

            Log.i(TAG, "END mAcceptThread");
        }

        public void cancel() {
            Log.d(TAG, "cancel: Canceling AcceptThread.");
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: Close of AcceptThread ServerSocket failed.");
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread: Starting.");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    String incomingMessage = new String(buffer, 0, bytes);
                    Log.d(TAG, "InputStream: " + incomingMessage);
                } catch (IOException e) {
                    Log.e(TAG, "run: Error reading inputstream. " + e.getMessage());
                    Intent intent = new Intent();
                    intent.setAction("com.example.solarmojo13.bartender.ACTION_BLUETOOTH_CONNECTED");
                    intent.putExtra("Connected", false);
                    mContext.sendBroadcast(intent);
                    break;
                }
            }

        }

        public void write(byte[] bytes) {
            String text = new String(bytes, Charset.defaultCharset());
            Log.d(TAG, "write: Writing to outputstream: " + text);
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(TAG, "write: Error writing to outputstream. " + e.getMessage());
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing socket");
            }
        }
    }
}
