package com.example.solarmojo13.bartender;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class ButtonAdapter extends RecyclerView.Adapter<ButtonAdapter.ButtonViewHolder>{
    public class ButtonViewHolder extends RecyclerView.ViewHolder {
        protected Button name;
        protected TextView cmnd;
        protected TextView type;
        protected CardView cv;

        public ButtonViewHolder(View v){
            super(v);
            name = (Button) v.findViewById(R.id.btnName);
            cmnd = (TextView)v.findViewById(R.id.action);
            type = (TextView)v.findViewById(R.id.type);
            cv = (CardView)v.findViewById(R.id.cv);
        }
    }
    private List<Command> commandList;
    public ButtonAdapter(List<Command> list){
        commandList = list;
    }

    public void setCommandList(List<Command> list){
        commandList = list;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount(){
        return commandList.size();
    }

    @Override
    public void onBindViewHolder(ButtonViewHolder buttonViewHolder,int i) {
        if (i < commandList.size()) {
            Command temp = commandList.get(i);
            buttonViewHolder.cmnd.setVisibility(View.INVISIBLE);
            buttonViewHolder.type.setVisibility(View.INVISIBLE);
            buttonViewHolder.name.setText(temp.getName());
            buttonViewHolder.name.setContentDescription(temp.getMessage());
        }

    }

    @Override
    public ButtonViewHolder onCreateViewHolder(ViewGroup group,int i){
        View item = LayoutInflater.from(group.getContext()).inflate(R.layout.item_command,group,false);
        return new ButtonViewHolder(item);
    }

}
