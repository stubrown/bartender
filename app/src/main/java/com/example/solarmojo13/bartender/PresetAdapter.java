package com.example.solarmojo13.bartender;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class PresetAdapter extends RecyclerView.Adapter<PresetAdapter.CustomViewHolder> {
    private ArrayList<Preset> presets;

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView presetName;

        public CustomViewHolder(View view) {
            super(view);
            presetName = (TextView) view.findViewById(R.id.presetName);
        }
    }

    public PresetAdapter(ArrayList<Preset> presets) {
        this.presets = presets;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_preset, parent, false);

        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.presetName.setText(presets.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return presets.size();
    }

}
