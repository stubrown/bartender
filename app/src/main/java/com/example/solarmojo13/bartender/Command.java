package com.example.solarmojo13.bartender;

import java.io.Serializable;

public class Command implements Serializable {
    String name;
    String type;
    String action;
    public Command(String name, String type, String action){
        this.name = name;
        this.type = type;
        this.action = action;
    }
    public Command(String name, String action){
        this.name = name;
        type = "Shortcut";
        this.action = action;
    }
    public String getName(){return name;}
    public String getType() {return type;}
    public String getAction() {return action;}
    public String getMessage(){return type + "," + action;}

    public void setName(String name) {
        this.name = name;
    }
    public void setAction(String action) {this.action = action;}
}
