package com.example.solarmojo13.bartender;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Spinner;

import java.io.File;
import java.util.ArrayList;

public class Main extends AppCompatActivity implements ConnectFragment.onBtnBTClickListener, SendFragment.onBtnCommandClickListener, PresetFragment.onPresetSelectListener {
    private static final String TAG = "MainActivity";

    //used to perform Bluetooth tasks
    BluetoothAdapter mBluetoothAdapter;
    //handles the initial connection and code when connected
    BluetoothConnectionService mBluetoothConnection;

    private ConnectFragment connectFragment;
    private PresetFragment presetFragment;
    private SendFragment sendFragment;

    private boolean onConnectFragment;
    private boolean onHelpFragment;
    private boolean onSendFragment;
    private boolean onPresetFragment;

    private Menu menu;
    private boolean editOffline;

    private final String UUID_FILE_NAME = "UUID.bartender";
    private String UUID_start;
    private GetFile UUIDfile;
    private ArrayList<Preset> presets;
    private int currentPresetNum;
    private GetFile presetFile;
    private final static String FILE_NAME = "presetstorage.bartender";


    //Detects changes in Bluetooth discoverability to ensure Bluetooth is enabled before doing anything
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {

                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch (mode) {
                    //When device is discoverable, begin connection process
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        Log.d(TAG, "BroadcastReceiver1: Discoverability Enabled.");
                        connectFragment.connectingUI();
                        startBTConnection();
                        break;

                    //Device not in discoverable mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        Log.d(TAG, "BroadcastReceiver1: Discoverability Disabled. Able to receive connections.");
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        Log.d(TAG, "BroadcastReceiver1: Discoverability Disabled. Not able to receive connections.");
                        //cancel current connection allow user to try connecting again if time runs out
                        if (onConnectFragment) {
                            if (mBluetoothConnection != null) {
                                mBluetoothConnection.cancel();
                            }
                            if (connectFragment!=null) {
                                connectFragment.enterUUIDUI();
                            }
                        }
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d(TAG, "BroadcastReceiver1: Connecting....");
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d(TAG, "BroadcastReceiver1: Connected.");
                        break;
                }

            }
        }
    };

    //receives message from Bluetooth connection thread once Bluetooth connection is established
    private final BroadcastReceiver mBroadcastReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "BroadcastReceiver2 triggered");
            final String action = intent.getAction();

            if (action.equals("com.example.solarmojo13.bartender.ACTION_BLUETOOTH_CONNECTED")) {
                boolean isConnected = intent.getBooleanExtra("Connected", false);
                //if connected, go to send fragment, otherwise go back to connect fragment
                if (isConnected) {
                    startPresetFragment();
                } else {
                    if (mBluetoothConnection!=null) {
                        mBluetoothConnection.cancel();
                    }
                    showDisconnectedAlert();
                }
            }
        }
    };




    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: called.");
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver1);
        unregisterReceiver(mBroadcastReceiver2);
        if (mBluetoothConnection!=null) {
            mBluetoothConnection.cancel();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "OnCreate called");
        setContentView(R.layout.activity_main);

        connectFragment = new ConnectFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, connectFragment);
        ft.commit();
        onConnectFragment = true;

        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(mBroadcastReceiver1, filter1);

        IntentFilter filter2 = new IntentFilter("com.example.solarmojo13.bartender.ACTION_BLUETOOTH_CONNECTED");
        registerReceiver(mBroadcastReceiver2, filter2);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        presetFile = new GetFile(this,FILE_NAME);
        if (presetFile.deserialize() != null) {
            presets = presetFile.deserialize();
        } else {
            new File(getFilesDir(), FILE_NAME);
            presets = new ArrayList<>();
            presets.add(new Preset("Default Preset", true));
            presetFile.serialize(presets);
        }
        UUIDfile = new GetFile(this,UUID_FILE_NAME);
        if (UUIDfile.deserialize() != null) {
            UUID_start = UUIDfile.deserializeUUID();
        } else {
            new File(getFilesDir(), UUID_FILE_NAME);
            UUID_start = "";
            UUIDfile.serialize(UUID_start);
        }

        editOffline = false;
    }


    //called from ConnectFragment to enable Bluetooth discoverability
    @Override
    public void startBT(String UUID_start, boolean bluetoothEnabled) {
        //startSendFragment(); //use this and comment out the rest of the method if you want to test without bluetooth
        if(!bluetoothEnabled)
            startPresetFragment();
        else {
            this.UUID_start = UUID_start;
            UUIDfile.serialize(this.UUID_start);
            Log.i(TAG, UUID_start);
            if (mBluetoothAdapter != null) {
                Log.d(TAG, "btnEnableDisable_Discoverable: Making device discoverable for 300 seconds.");

                Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                startActivity(discoverableIntent);
            } else {
                Log.e(TAG, "Device does not support Bluetooth.");
            }
        }
    }
    //called once Bluetooth is enabled (notified via BroadcastReceiver1)
    public void startBTConnection(){
        Log.d(TAG, "startBTConnection: Initializing RFCOMM Bluetooth Connection.");
        mBluetoothConnection = new BluetoothConnectionService(Main.this, UUID_start);
    }
    //called when cancel button is pressed
    public void cancelBTConnection() {
        if (mBluetoothConnection!=null) {
            mBluetoothConnection.cancel();
        }
    }


    //called from SendFragment once button is pressed
    @Override
    public void sendCommand(String command) {
        if (mBluetoothConnection != null) {
            mBluetoothConnection.write(command.getBytes());
            Log.i(TAG, "Send command attempted");
        }
    }


    //called once Bluetooth connection is established (notified via BroadcastReceiver2)
    public void startPresetFragment() {
        Log.i(TAG, "Start PresetFragment");
        presetFragment = new PresetFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, presetFragment);
        ft.commit();
        connectFragment = null;
        onConnectFragment = false;
        onPresetFragment = true;
        onSendFragment = false;
        onHelpFragment = false;
        offlineIconVisibility();
    }
    //called if Bluetooth connection is severed (notified via BroadcastReceiver2)
    public void startConnectFragment() {
        if (mBluetoothConnection!=null) {
            mBluetoothConnection.cancel();
        }
        connectFragment = new ConnectFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, connectFragment);
        ft.commit();
        onConnectFragment = true;
        onPresetFragment = false;
        onSendFragment = false;
        onHelpFragment = false;
        offlineIconVisibility();
    }
    //called when preset is selected on PresetFragment
    public void startSendFragment() {
        Log.i(TAG, "Start SendFragment");
        sendFragment = new SendFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, sendFragment);
        ft.commit();
        connectFragment = null;
        onConnectFragment = false;
        onPresetFragment = false;
        onSendFragment = true;
        onHelpFragment = false;
        offlineIconVisibility();
    }
    public void startHelpFragment() {
        HelpFragment helpFragment = new HelpFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, helpFragment);
        ft.commit();
        connectFragment = null;
        onHelpFragment = true;
        offlineIconVisibility();
    }

    @Override
    public void goToPreset(int presetNum) {
        currentPresetNum = presetNum;
        startSendFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help:
                startHelpFragment();
                return true;
            case R.id.toggleoffline:
                editOffline = !editOffline;
                updateOfflineIcon();
                connectFragment.editOffline(editOffline);
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.findItem(R.id.toggleoffline).setVisible(onConnectFragment&&!onHelpFragment);

        return super.onPrepareOptionsMenu(menu);
    }

    public void offlineIconVisibility() {
        Log.i(TAG, "ActionBar updated");
        if (onConnectFragment && !onHelpFragment) {
           //item.setVisible(true);
            invalidateOptionsMenu();
        } else {
            Log.i(TAG, "Not Visible");
            invalidateOptionsMenu();
        }
    }
    public void updateOfflineIcon() {
        if (editOffline) {
            menu.findItem(R.id.toggleoffline).setIcon(R.drawable.ic_wifioff);
        } else {
            menu.findItem(R.id.toggleoffline).setIcon(R.drawable.ic_wifi);
        }
    }


    @Override
    public ArrayList<Preset> getPresets() {
        return presets;
    }
    @Override
    public Preset getCurrentPreset() {
        return presets.get(currentPresetNum);
    }
    @Override
    public String getUUID(){
        if(UUID_start!=null)
            return UUID_start;
        return "";
    }


    public void showAboutToDisconnectAlert() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder .setMessage("Returning to the connection screen will cause the connection to be lost. Do you want to continue?")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mBluetoothConnection != null) {
                            mBluetoothConnection.cancel();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setCancelable(false)
                .show();

    }
    public void showDisconnectedAlert() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder .setMessage("The connection has been lost. Returning to connection screen.")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startConnectFragment();
                    }
                })
                .setCancelable(false)
                .show();
    }
    @Override
    public void editPresetAlert(int pos) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_newpreset, null);
        final EditText presetNameET  = (EditText) alertLayout.findViewById(R.id.presetName);
        presetNameET.setHint(presets.get(pos).getName());
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Edit Preset");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        final int position = pos;
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (presetNameET.getText().length()!=0) {
                    presets.get(position).setName(presetNameET.getText().toString());
                    presetFile.serialize(presets);
                    presetFragment.update();
                }
            }
        });
        alert.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presets.remove(position);
                presetFile.serialize(presets);
                presetFragment.update();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }
    @Override
    public void newPresetAlert() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_newpreset, null);
        final EditText presetNameET  = (EditText) alertLayout.findViewById(R.id.presetName);
        presetNameET.setHint("Preset " + (presets.size()+1));
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("New Preset");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (presetNameET.getText().length()<=0) {
                    presets.add(new Preset(presetNameET.getHint().toString()));
                    presetFile.serialize(presets);
                    presetFragment.update();
                } else {
                    presets.add(new Preset(presetNameET.getText().toString()));
                    presetFile.serialize(presets);
                    presetFragment.update();
                }
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }
    @Override
    public void newButtonAlert() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_newbutton, null);
        final EditText nameET  = (EditText) alertLayout.findViewById(R.id.btnName);
        final Spinner spinner = (Spinner) alertLayout.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.types_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        final EditText commandET = (EditText) alertLayout.findViewById(R.id.btnCommand);
        final ImageButton addChar = (ImageButton) alertLayout.findViewById(R.id.addChar);
        final Context context = this;
        addChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.specialchar, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                         String newCommand = commandET.getText().toString()+item.getTitleCondensed();
                         commandET.setText(newCommand);
                         commandET.setSelection(commandET.getText().length());
                         return true;
                    }
                });
                popup.show();
            }
        });
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("New Button");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presets.get(currentPresetNum).addCommand(new Command(nameET.getText().toString(), spinner.getSelectedItem().toString(), commandET.getText().toString()));
                presetFile.serialize(presets);
                sendFragment.update();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }
    @Override
    public void editButtonAlert(CardView cv, int pos) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_newbutton, null);
        final EditText nameET  = (EditText) alertLayout.findViewById(R.id.btnName);
        final Spinner spinner = (Spinner) alertLayout.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.types_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        final EditText commandET = (EditText) alertLayout.findViewById(R.id.btnCommand);
        final ImageButton addChar = (ImageButton) alertLayout.findViewById(R.id.addChar);
        final Context context = this;
        addChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.specialchar, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String newCommand = commandET.getText().toString()+item.getTitleCondensed();
                        commandET.setText(newCommand);
                        commandET.setSelection(commandET.getText().length());
                        return true;
                    }
                });
                popup.show();
            }
        });
        nameET.setText(presets.get(currentPresetNum).getCommand(pos).getName());
        spinner.setSelection(adapter.getPosition(presets.get(currentPresetNum).getCommand(pos).getType()));
        commandET.setText(presets.get(currentPresetNum).getCommand(pos).getAction());
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Edit Button");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        final CardView view = cv;
        final int position = pos;
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = nameET.getText().toString();
                if (name.length()==0) {
                    name = nameET.getHint().toString();
                }
                String type = spinner.getSelectedItem().toString();
                String command = commandET.getText().toString();
                if (command.length()==0) {
                    command = commandET.getHint().toString();
                }
                presets.get(currentPresetNum).setCommand(position, new Command(name, type, command));
                sendFragment.setCommandToView(view, new Command(name, type, command));
                presetFile.serialize(presets);

            }
        });
        alert.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presets.get(currentPresetNum).removeCommand(position);
                presetFile.serialize(presets);
                sendFragment.update();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }




    @Override
    public void onBackPressed() {
        if (onHelpFragment) {
            if (onSendFragment) {
                startSendFragment();
            } else if (onPresetFragment) {
                startPresetFragment();
            } else {
                startConnectFragment();
            }
        } else {
            if (onSendFragment) {
                startPresetFragment();
            } else if (onPresetFragment) {
                //notifies connection will be lost and then starts connect fragment
                if (!editOffline) {
                    showAboutToDisconnectAlert();
                } else {
                    startConnectFragment();
                }
            }
        }
    }

}