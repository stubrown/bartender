package com.example.solarmojo13.bartender;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HelpFragment extends Fragment {

    private Context context;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        getActivity().setTitle("Help");

        ViewGroup layout = (ViewGroup) view.findViewById(R.id.layout);
        for (int i=0; i<layout.getChildCount(); i++) {
            ((ViewGroup)((ViewGroup)layout.getChildAt(i)).getChildAt(0)).getChildAt(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View tv = ((ViewGroup) view.getParent()).getChildAt(1);
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) tv.getLayoutParams();
                    if (params.height!=0) {
                        params.height = 0;
                        params.setMargins(0, 0, 0, 0);
                    } else {
                        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        params.setMargins(convertDpToPixels(8), 0, convertDpToPixels(8), convertDpToPixels(8));
                    }
                    tv.setLayoutParams(params);
                }
            });
        }
        return view;
    }

    public int  convertDpToPixels(int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }
}
