package com.example.solarmojo13.bartender;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import static android.view.Gravity.CENTER_HORIZONTAL;
import static android.view.Gravity.LEFT;
import static android.view.Gravity.RIGHT;

public class ConnectFragment extends Fragment {
    private static final String TAG = "ConnectFragment";

    //interface to communicate with main activity
    private onBtnBTClickListener btnBTClickListener;

    //starts and cancels connection
    private Button btnBT;

    private EditText editText;
    private boolean  connecting = false;
    private boolean editOffline;

    AnimationDrawable anim;

    //enables connection button once code is the correct length
    private TextWatcher codeTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length()==6) {
                btnBT.setEnabled(true);
                btnBT.setTextColor(Color.parseColor("#636363"));
            } else {
                btnBT.setEnabled(false);
                btnBT.setTextColor(Color.parseColor("#999999"));
            }

            if (s.length()==0) {
                editText.setGravity(LEFT);
            } else {
                editText.setGravity(CENTER_HORIZONTAL);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            btnBTClickListener = (onBtnBTClickListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "Main activity must implement onBtnBTClickListener");
            //throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG, "ConnectFragment: OnCreate called");
        View view = inflater.inflate(R.layout.fragment_connect, container, false);
        btnBT = (Button) view.findViewById(R.id.btnDiscoverable_on_off);
        /*bluetoothButton = (ImageButton) view.findViewById(R.id.imgbtnBluetooth);
        if(bluetooth){
            bluetoothButton.setImageResource(R.drawable.bluetoothdefault);
        }
        else{
            bluetoothButton.setImageResource(R.drawable.bluetoothdisable);
        }*/
        setHasOptionsMenu(true);

        anim=(AnimationDrawable)btnBT.getCompoundDrawables()[0];
        //button performs different actions depending on state
        getActivity().setTitle("Bartender");
        btnBT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (btnBTClickListener != null) {
                    if (!connecting) {
                        btnBTClickListener.startBT(editText.getText().toString(), !editOffline);
                    } else {
                        btnBTClickListener.cancelBTConnection();
                        enterUUIDUI();
                    }
                }
            }
        });


        btnBT.setEnabled(false);
        btnBT.setTextColor(Color.parseColor("#999999"));

        editText = (EditText) view.findViewById(R.id.editText);
        editText.addTextChangedListener(codeTextWatcher);
        //hide keyboard after clicking outside it
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        editText.setText(btnBTClickListener.getUUID());
        Log.i(TAG, btnBTClickListener.getUUID());
        Log.i(TAG, editText.getText().toString());
        getActivity().setTitle("Bartender");

        return view;
    }

    public void editOffline (boolean edit) {
        if(edit){
            editOffline = edit;
            editText.setEnabled(false);
            btnBT.setEnabled(true);
            btnBT.setTextColor(Color.parseColor("#636363"));
            btnBT.setText("Edit Offline");
            btnBT.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            Log.v(TAG,"Disabling Bluetooth");
        }
        else {
            editText.setEnabled(true);
            if (editText.getText().toString().length()<6) {
                btnBT.setEnabled(false);
                btnBT.setTextColor(Color.parseColor("#999999"));
                btnBT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.animation_connection_blank, 0, 0, 0);
            }
            btnBT.setText("Connect");
            Log.v(TAG, "Enabling Bluetooth");
        }
    }

    public void connectingUI() {
        editText.setEnabled(false);
        btnBT.setText("Cancel");
        anim.stop();
        btnBT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.animation_connection_loading,0,0,0);
        anim = (AnimationDrawable)btnBT.getCompoundDrawables()[0];
        anim.start();
        connecting = true;
    }

    public void enterUUIDUI() {
        editText.setEnabled(true);
        btnBT.setText("Connect");
        anim.stop();
        btnBT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.animation_connection_blank,0,0,0);
        anim = (AnimationDrawable)btnBT.getCompoundDrawables()[0];
        anim.start();
        connecting = false;
    }


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    //interface to communicate with main activity
    public interface onBtnBTClickListener {
        void startBT(String UUID_start,boolean bluetoothEnable);
        String getUUID();
        void cancelBTConnection();
    }
}
