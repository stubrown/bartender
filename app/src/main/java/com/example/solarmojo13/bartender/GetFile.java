package com.example.solarmojo13.bartender;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class GetFile {
    //stores the object to a file
    private String path;
    private final String TAG = "GETFILE";
    private Context context;
    public GetFile(Context context, String path){
        this.context = context;
        this.path = path;
    }
    public void serialize(String UUID){
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
        try{
            //storing the file
            fout = context.openFileOutput(path, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(UUID);
        }
        catch(Exception e){
            Log.e(TAG,e.toString());
        }
        finally {
            //closing the streams after writing the file
            if(fout!=null){
                try{
                    fout.close();
                } catch (Exception e){
                    Log.e(TAG,e.toString());
                }
            }
            if(oos!=null){
                try{
                    oos.close();
                } catch (Exception e){
                    Log.e(TAG,e.toString());
                }
            }
        }
    }
    public void serialize(ArrayList<Preset> commands){
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
        try{
            //storing the file
            fout = context.openFileOutput(path, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(commands);
        }
        catch(Exception e){
            Log.e(TAG,e.toString());
        }
        finally {
            //closing the streams after writing the file
            if(fout!=null){
                try{
                    fout.close();
                } catch (Exception e){
                    Log.e(TAG,e.toString());
                }
            }
            if(oos!=null){
                try{
                    oos.close();
                } catch (Exception e){
                    Log.e(TAG,e.toString());
                }
            }
        }
    }
    //reads the object from the file
    public ArrayList<Preset> deserialize(){
        ArrayList<Preset> storage = null;
        FileInputStream fin = null;
        ObjectInputStream ois = null;
        try{
            //reading the file
            fin = context.openFileInput(path);
            ois = new ObjectInputStream(fin);
            storage = (ArrayList<Preset>) ois.readObject();
        }
        catch(Exception e){
            Log.e(TAG,e.toString());
        }
        finally {
            //closing the streams after reading
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return storage;
    }
    public String deserializeUUID(){
        String storage = null;
        FileInputStream fin = null;
        ObjectInputStream ois = null;
        try{
            //reading the file
            fin = context.openFileInput(path);
            ois = new ObjectInputStream(fin);
            storage = (String) ois.readObject();
        }
        catch(Exception e){
            Log.e(TAG,e.toString());
        }
        finally {
            //closing the streams after reading
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return storage;
    }

}
