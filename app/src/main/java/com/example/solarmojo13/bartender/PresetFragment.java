package com.example.solarmojo13.bartender;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.io.File;
import java.util.ArrayList;

public class PresetFragment extends Fragment {

    private static final String TAG = "PresetFragment";

    //interface to communicate with main activity
    private onPresetSelectListener presetSelectListener;
    private Context context;

    private ArrayList<Preset> presetList;

    private RecyclerView recyclerView;
    private PresetAdapter adapter;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        Log.i(TAG, "PresetFragment Attached");
        try {
            presetSelectListener = (PresetFragment.onPresetSelectListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "Main activity must implement onPresetSelectListener");
            //throw new ClassCastException(activity.toString() + " must implement onBtnCommandClickListener");
        }
        context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preset, container, false);
        Log.i(TAG, "PresetFragment onCreateView");

        getActivity().setTitle("Presets");
        presetList = presetSelectListener.getPresets();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        adapter = new PresetAdapter(presetList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(ContextCompat.getDrawable(context,
                        R.drawable.item_separator)));
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(context,
                recyclerView, new RecyclerItemListener.RecyclerTouchListener() {
            @Override
            public void onClickItem(View v, int position) {
                Log.i(TAG, "Preset at index " + position + " clicked");
                presetSelectListener.goToPreset(position);
            }

            @Override
            public void onLongClickItem(View v, int position) {
                presetSelectListener.editPresetAlert(position);
            }
        }));

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addPresetBtn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presetSelectListener.newPresetAlert();
            }
        });
        return view;

    }

    public void update() {
        presetList = presetSelectListener.getPresets();
        adapter.notifyDataSetChanged();
    }


    //interface to communicate with main activity
    public interface onPresetSelectListener {
        void newPresetAlert();
        void editPresetAlert(int position);
        void goToPreset(int presetNum);
        ArrayList<Preset> getPresets();
    }

}
