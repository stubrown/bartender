package com.example.solarmojo13.bartender;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SendFragment extends Fragment {
    private static final String TAG = "SendFragment";

    //interface to communicate with main activity
    private onBtnCommandClickListener btnCommandClickListener;

    private Context context;

    private Preset currentPreset;
    private CommandAdapter adapter;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "SendFragment Attached");
        try {
            btnCommandClickListener = (onBtnCommandClickListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "Main activity must implement onBtnCommandClickListener");
            //throw new ClassCastException(activity.toString() + " must implement onBtnCommandClickListener");
        }
        context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send, container, false);
        Log.i(TAG, "SendFragment onCreateView");

        //setup UI and get currentPreset
        RecyclerView recView = (RecyclerView) view.findViewById(R.id.recycler_view);
        currentPreset = btnCommandClickListener.getCurrentPreset();
        adapter = new CommandAdapter( currentPreset.getCommands());
        recView.setLayoutManager(new LinearLayoutManager(context));
        recView.setItemAnimator(new DefaultItemAnimator());
        recView.setAdapter(adapter);
        recView.setItemAnimator(new DefaultItemAnimator());
        getActivity().setTitle(currentPreset.getName());


        recView.addOnItemTouchListener(new RecyclerItemListener(context,
                recView, new RecyclerItemListener.RecyclerTouchListener() {
            @Override
            public void onClickItem(View v, int position) {
                Log.i(TAG, "Preset at index " + position + " clicked");
                CardView cv = (CardView) v;
                LinearLayout rl = (LinearLayout)cv.getChildAt(0);
                Button btn = (Button)rl.getChildAt(0);
                Log.i(TAG, "Button command: " + btn.getContentDescription() + "");
                btnCommandClickListener.sendCommand("" + btn.getContentDescription());
            }

            @Override
            public void onLongClickItem(View v, int position) {
                btnCommandClickListener.editButtonAlert((CardView) v, position);
            }
        }));

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addBtn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnCommandClickListener.newButtonAlert();
            }
        });
        update();
        return view;
    }

    public void setCommandToView(CardView cv, Command command) {
         Button btn = (Button) cv.findViewById(R.id.btnName);
         btn.setText(command.getName());
         btn.setContentDescription(command.getMessage());
         TextView type = (TextView) cv.findViewById(R.id.type);
         type.setText(command.getType());
         TextView action = (TextView) cv.findViewById(R.id.action);
         action.setText(command.getAction());
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void update() {
        adapter.notifyDataSetChanged();
    }


    //interface to communicate with main activity
    public interface onBtnCommandClickListener {
        void sendCommand(String command);
        Preset getCurrentPreset();
        void newButtonAlert();
        void editButtonAlert(CardView cv, int pos);
    }
}

