package com.example.solarmojo13.bartender;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

//stores the commands and buttons for each list to be able to be written to a file
public class Preset implements Serializable {
    private String name;

    private ArrayList<Command> commands;

    public Preset() {
        commands = new ArrayList<>();
    }
    public Preset(String name) {
        this.name = name;
        commands = new ArrayList<>();
    }
    public Preset(String name, boolean defaultPreset) {
        this.name = name;
        commands = new ArrayList<>();
        if (defaultPreset) {
            //default commands
            commands.add(new Command("Ctrl + A", "^a"));
            commands.add(new Command("Ctrl + C", "^c"));
            commands.add(new Command("Ctrl + V", "^v"));
            commands.add(new Command("Ctrl + S", "^s"));
            commands.add(new Command("Ctrl + O", "^o"));
            commands.add(new Command("Ctrl + N", "^n"));
            commands.add(new Command("Ctrl + T", "^t"));
            commands.add(new Command("Ctrl + F", "^f"));
        }
    }
    public Preset(ArrayList<Command> commands) {
        this.commands = commands;
    }


    public String getName() {
        return name;
    }
    public int getSize() {
        return commands.size();
    }
    public ArrayList<Command> getCommands() {
        return commands;
    }
    public Command getCommand(int i) {
        if (i< commands.size()&&i>=0) {
            return commands.get(i);
        } else {
            return null;    //might want to return empty command instead
        }
    }

    public void setName(String name) {
        this.name = name;
    }
    public void addCommand(Command command) {
        commands.add(command);
    }
    public void setCommand(int i, Command command) {
        if (i< commands.size()&&i>=0) {
            commands.set(i, command);
        }
    }
    public void removeCommand(int i) {
        if (i< commands.size()&&i>=0) {
            commands.remove(i);
        }
    }
}
