package com.example.solarmojo13.bartender;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class CommandAdapter extends RecyclerView.Adapter<CommandAdapter.CommandViewHolder> {

    private ArrayList<Command> commands;

    public class CommandViewHolder extends RecyclerView.ViewHolder {
        protected Button name;
        protected TextView action;
        protected TextView type;

        public CommandViewHolder(View view) {
            super(view);
            name = (Button) view.findViewById(R.id.btnName);
            action = (TextView) view.findViewById(R.id.action);
            type = (TextView) view.findViewById(R.id.type);
        }
    }

    public CommandAdapter(ArrayList<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount(){
        return commands.size();
    }

    @Override
    public void onBindViewHolder(CommandAdapter.CommandViewHolder holder, int position) {
        holder.name.setText(commands.get(position).getName());
        holder.type.setText(commands.get(position).getType());
        holder.action.setText(commands.get(position).getAction());
        holder.name.setContentDescription(commands.get(position).getMessage());
    }

    @Override
    public CommandViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_command, parent, false);

        return new CommandAdapter.CommandViewHolder(itemView);
    }

}
