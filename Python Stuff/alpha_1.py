
'''FIXES BLURRY TEXT'''
import ctypes
# Query DPI Awareness (Windows 10 and 8)
awareness = ctypes.c_int()
errorCode = ctypes.windll.shcore.GetProcessDpiAwareness(0, ctypes.addressof(awareness))

# Set DPI Awareness  (Windows 10 and 8)
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)
# the argument is the awareness level, which can be 0, 1 or 2:
# for 1-to-1 pixel control I seem to need it to be non-zero (I'm using level 2)

# Set DPI Awareness  (Windows 7 and Vista)
success = ctypes.windll.user32.SetProcessDPIAware()



import sys
import os
import bluetooth
import win32com.client
import uuid
import pyqrcode
from PIL import ImageTk, Image
import Tkinter as tk
import ttk
import threading
import random as rand
from random import randint

'''global vars'''
#for asynchronous Bluetooth scanning. If only method call, Tkinter window will freeze up/Not Responding until scan concluded
global scanningThread
global port
global name
global host
global sock
global resetThread
global uuid

resetThread = False

shell = win32com.client.Dispatch("WScript.Shell") #initiates the Windows OS command interface

''' EXAMPLES OF KEYSTROKES
#shell.SendKeys("^a") # CTRL+A may "select all" depending on which window's focused
#shell.SendKeys("{DELETE}") # Delete selected text?  Depends on context. :P
#shell.SendKeys("{TAB}") #Press tab... to change focus or whatever
shell.SendKeys("^+{ESC}")
'''


def genKey(n): #3/16/18 - changed to numbers only
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return str(randint(range_start, range_end))
    #return uuid.uuid4().hex[:length];

#UUID GENERATION~
uuid
short = ""
rand = genKey(6)

if (os.path.isfile('save.txt') == False or os.stat("save.txt").st_size == 0):
    with open("save.txt","w") as f: #write
        f.write(rand)
        short = rand
else:
    with open("save.txt") as f: #read
        short = f.readlines()[0]

uuid = short + "55-9e19-4040-a26b-886edb003fa6"
print uuid

def regenUUID():
    global uuid
    rand = genKey(6)
    with open("save.txt","w") as f: #write
        f.write(rand)
        uuid = rand + "55-9e19-4040-a26b-886edb003fa6"
        uuidTxt["text"] = rand

#DEFAULT: TESTING uuid = "42679155-9e19-4040-a26b-886edb003fa6"

#uuid = str(uuid.uuid4())

#Generate QR code based on UUID
'''
qrPath = "uuid-src.png"
url = pyqrcode.create(uuid)
url.png(qrPath, scale=8)
'''

def scanForService(): #Synchronous scan for device, made asynchronous by initiateScan method. DO NOT DIRECTLY CALL WITHOUT ASYNC
    global scanningThread
    global port
    global name
    global host
    global sock
    global uuid
    global resetThread
    if (resetThread == True):
        resetThread = False
        pass
    print "Scanning for BT services matching " + uuid + "..."
    scanBtn["text"] = "Scanning..."
    scanBtn['state'] = 'disabled'
    scanBtn.config(font=('arial', 11, 'italic bold'))

    service_matches = bluetooth.find_service( uuid = uuid )
    if len(service_matches) == 0:
        print "Device not found..."
        scanBtn["text"] = "Not found, Retry?"
        scanBtn['state'] = 'normal'
        scanBtn.config(font=('arial', 11, 'bold'))
        return

    first_match = service_matches[0]
    port = first_match["port"]
    name = first_match["name"]
    host = first_match["host"]

    print "Connecting to " + name + " " + host + "..."

    sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
    sock.connect((host, port))

    print "Connection success!"
    if (len(name) > 10):
        statusTxt["text"] = name[:10] + "..."
    else:
        statusTxt["text"] = name
    scanBtn["text"] = "Disconnect"
    scanBtn.config(font=('arial', 12, 'bold'))
    scanBtn['state'] = 'normal'
    scanBtn['command'] = closeConnection
    #For now, button will disable upon succesfull connection for simplicity

    listen(sock)

    #sock.send("Test")
    #sock.close()



'''C O M M A N D S - cmd parameter

    shortcut:
        Presses key combination based on context, e.g. "^a" for Ctrl-A
        ____SYNTAX____
            ^   ctrl
            +   shift
            {}  keyword keys, e.g. {DEL} or {TAB} or

    run:
        Starts a previously not-running program in Windows, e.g. "cmd.exe"

    time:      (?) client or computer
        Pauses for a specificied time

    focus:
        Brings a previously running (or not? TBD, investigate) program to to the forefront, as if you pressed it in the taskbar

'''

def executeCommand(cmd, ctx): #cmd: command, ctx: context. Command determines method called, context determines parameters
    if cmd == "Shortcut":
        if (ctx == "rickroll2018"):
            dead_meme()
        else:
            shell.SendKeys(ctx)
    elif cmd == "Run":
        shell.run(ctx)
    elif cmd == "Pause": #allows for timing when chaining keys, so that eventually the user can chain commands with time inbetween each other
        ''' #SOON TO BE DEPRECATED: BETTER TO DO ON ANDROID SIDE
            EXAMPLE: Run notepad, pause for 5 seconds to allow the program to open before typing, then type the letter "q"
                1: run,notepad.exe
                2: pause,5
                3: shortcut,q
        '''
        shell.sleep(int(ctx))

def listen(sock):
    global port
    global name
    global host
    '''
    ATM, sock.recv is SYNCHRONOUS, blocks everything else until data recieved.
    Shouldn't matter for now, unlike service-scanning, user does not need to interact with the Tkinter GUI once connected
    Leaves the problem of how to disconnect? Investigate running background applications, possibly right click tray icon to open menu w/ exit button
    '''
    data = sock.recv(1024)
    print data
    #decode = "".join(map(chr, data))
    if len(data) == 0: #accounts for crashes
        print "Disconnected from device!"
        closeConnection()
        return
    res = data.split(',')
    resetThread = True
    cmd = res[0]
    ctx = res[1]
    executeCommand(cmd, ctx)
    try:
        listen(sock) #recurse, listen for next packet of info
    except Exception as e:
        resetGUI()

def resetGUI():
    statusTxt["text"] = "Disconnected."
    scanBtn["text"] = "Rescan"
    scanBtn.config(font=('arial', 11, 'bold'))
    scanBtn["command"] = initiateScan
    scanBtn['state'] = 'normal'

def initiateScan(): #asynchronous method call straight from GUI to avoid blocking Tkinter thread
    global scanningThread
    scanningThread = threading.Thread(target=scanForService, args=(), kwargs={})
    scanningThread.start()

def closeConnection():
    global scanningThread
    global resetThread
    global sock
    resetGUI()
    sock.close()
    resetThread = True

def exitProgram():
    window.destroy()
    sys.exit(0)

#TESTING COMMANDS (INTERNAL)================================

import time
def dead_meme():
    executeCommand("shortcut", "{ESCAPE}")
    time.sleep(3)
    executeCommand("run", "notepad.exe")
    time.sleep(3)
    executeCommand("shortcut", "~{enter}")
    time.sleep(2)
    executeCommand("shortcut", "We're no strangers to love...")
    time.sleep(2)
    executeCommand("shortcut", "{ENTER}You know the rules, and so do I")
    time.sleep(1)
    executeCommand("shortcut", "{ENTER}A fuLl commitment's what I'm... thinking of")
    time.sleep(1)
    executeCommand("shortcut", "{ENTER}You wouldn't get this from any other guy{ENTER}")
    time.sleep(1)
    executeCommand("shortcut", "{ENTER}Never gonna give you up")
    time.sleep(1)
    executeCommand("shortcut", "{ENTER}Never gonna let you down")
    time.sleep(1)
    executeCommand("shortcut", "{ENTER}Never gonna run around")
    time.sleep(1)
    executeCommand("shortcut", " or desert you")
    time.sleep(3)
    executeCommand("shortcut", "^s")
    time.sleep(1)
    executeCommand("shortcut", "dead_meme_"+genKey(5)+".txt")
    time.sleep(3)
    executeCommand("shortcut", "{ENTER}")
    time.sleep(5)
    executeCommand("run", "chrome.exe");
    time.sleep(2)
    executeCommand("shortcut", "^t")
    time.sleep(1)
    executeCommand("shortcut", "https://www.youtube.com/watch?v=dQw4w9WgXcQ");
    executeCommand("shortcut", "{ENTER}");
    time.sleep(7)
    executeCommand("shortcut", "+>f{UP}{UP}{UP}{UP}");
    time.sleep(7)
    executeCommand("shortcut", "+>");
    time.sleep(7)
    executeCommand("shortcut", "+>");
    time.sleep(7)
    executeCommand("shortcut", "+<+<+<+<+<+<+<+<+<+<");
    time.sleep(13)
    executeCommand("shortcut", "^w")


#=============================================

#TK STYLING---------

class GradientFrame(tk.Canvas):
    '''A gradient frame which uses a canvas to draw the background'''
    def __init__(self, parent, borderwidth=1, relief="sunken"):
        tk.Canvas.__init__(self, parent, borderwidth=borderwidth, relief=relief)
        self._color1 = "#6da5ff"
        self._color2 = "#6da5ff"
        self.bind("<Configure>", self._draw_gradient)

    def _draw_gradient(self, event=None):
        '''Draw the gradient'''
        self.delete("gradient")
        width = self.winfo_width()
        height = self.winfo_height()
        limit = width
        (r1,g1,b1) = self.winfo_rgb(self._color1)
        (r2,g2,b2) = self.winfo_rgb(self._color2)
        r_ratio = float(r2-r1) / limit
        g_ratio = float(g2-g1) / limit
        b_ratio = float(b2-b1) / limit

        for i in range(limit):
            nr = int(r1 + (r_ratio * i))
            ng = int(g1 + (g_ratio * i))
            nb = int(b1 + (b_ratio * i))
            color = "#%4.4x%4.4x%4.4x" % (nr,ng,nb)
            self.create_line(i,0,i,height, tags=("gradient",), fill=color)
        self.lower("gradient")


#TK MAIN-----------
import tempfile

ICON = (b'\x00\x00\x01\x00\x01\x00\x10\x10\x00\x00\x01\x00\x08\x00h\x05\x00\x00'
        b'\x16\x00\x00\x00(\x00\x00\x00\x10\x00\x00\x00 \x00\x00\x00\x01\x00'
        b'\x08\x00\x00\x00\x00\x00@\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        b'\x00\x01\x00\x00\x00\x01') + b'\x00'*1282 + b'\xff'*64

_, ICON_PATH = tempfile.mkstemp()
with open(ICON_PATH, 'wb') as icon_file:
    icon_file.write(ICON)


window = tk.Tk()
window.iconbitmap(default=ICON_PATH)

'''
window.wm_overrideredirect(True)
gradient_frame = GradientFrame(window)
gradient_frame.pack(side="top", fill="both", expand=True)
inner_frame = tk.Frame(gradient_frame, height=50, bg="#6da5ff")
inner_frame.pack(side="top", fill="both", expand=True)
'''

window.title(" ")
window.configure(background='white')
window.resizable(False, False)
#window.im1 = Image.open(qrPath)

uuidTxt = tk.Label(window, pady="20", anchor="n", text=short, bg="white", fg="black", height="1", font=("consolas", 20),  cursor="ibeam")
uuidTxt.pack(fill="both", expand="True", side="top")

regenBtn = tk.Button(window, width=8, text="Reset", fg="#6da5ff", bg="white", activeforeground="black", disabledforeground="white", border="0", height="1")
regenBtn.pack(side="top")
regenBtn.config(font=('arial', 11, ''))

'''quitBtn = tk.Button(window, text="Exit", fg="white", bg="red", activeforeground="black", activebackground="black", disabledforeground="#f4f4f4", border="1", height="1")
quitBtn.pack(side="bottom", fill="x")
quitBtn.config(font=('arial', 11, 'bold'))'''

'''
img = ImageTk.PhotoImage(Image.open(qrPath).resize((800, 800), Image.NEAREST)) #qr code image
panel = tk.Label(window, image=img, anchor="s", bg="white")
panel.pack(fill="both", expand=True, side="top")
'''

statusTxt = tk.Label(window, pady="40", padx="90", anchor="n", bg="white", text="Disconnected.", font=("arial", 12))
statusTxt.pack(fill="both", expand=False, side="top")

scanBtn = tk.Button(window, text="Scan", fg="white", bg="#6da5ff", padx="20", activeforeground="#f4f4f4", activebackground="#5684ce", disabledforeground="#f4f4f4", border="0", height="2")
scanBtn.pack(side="top", fill="x")
scanBtn.config(font=('arial', 11, 'bold'))

cancelBtn = tk.Button(window, text="cancel", fg="#7f8187", bg="#e3e7ed", padx="20", activeforeground="#f4f4f4", activebackground="#c1c5cc", disabledforeground="white", border="0", height="1")
cancelBtn.pack(side="top", fill="x")
cancelBtn.config(font=('arial', 10, ''))

#quitBtn["command"] = exitProgram
scanBtn["command"] = initiateScan #lambda? tbd
cancelBtn["command"] = closeConnection
regenBtn["command"] = regenUUID

window.style = ttk.Style()
window.style.theme_use("clam")

def on_closing():
    print "Closing..."
    exitProgram();

window.protocol("WM_DELETE_WINDOW", on_closing)

window.mainloop() #builds GUI
#TK STUFF END--------------