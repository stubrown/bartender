
import sys
import bluetooth

uuid = "42679155-9e19-4040-a26b-886edb003fa6"
service_matches = bluetooth.find_service( uuid = uuid )

if len(service_matches) == 0:
    print "couldn't find the HotBar service"
    sys.exit(0)

first_match = service_matches[0]
port = first_match["port"]
name = first_match["name"]
host = first_match["host"]
foundmatch = False; 

for match in service_matches:
    name = match["name"]
    inp = raw_input("Are you looking for " + name + "? Y/N")
    if(inp == "Y"):
        port = match["port"]
        host = match["host"]
        foundmatch = True

if (foundmatch):
    sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
    sock.connect((host, port))
    print "Connected"
    sock.settimeout(60);
    data = sock.recv(1024)
    print "received [%s]" % data
    
else:
    print "No match found"

print "-------------------------------"